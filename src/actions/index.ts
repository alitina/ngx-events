
import { Action } from '@ngrx/store';

export enum EventsComponentActionTypes {
    SetView = '[Events Component] Set view',
    SetTable = '[Events Component] Set table',
    SetCalendar = '[Events Component] Set calendar',
}


export class SetView implements Action {
    readonly type = EventsComponentActionTypes.SetView;
    constructor(public readonly payload: any) { }
}

export class SetTable implements Action {
    readonly type = EventsComponentActionTypes.SetTable;
    constructor(public readonly payload: any) { }
}

export class SetCalendar implements Action {
    readonly type = EventsComponentActionTypes.SetCalendar;
    constructor(public readonly payload: any) { }
}

export type EventsComponentActions = SetView | SetTable | SetCalendar;
