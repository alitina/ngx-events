import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  OnChanges,
  Output,
  ViewChild,
  SimpleChanges,
  ViewEncapsulation,
  AfterViewInit,
  Optional
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable, ResponseError } from '@themost/client';
import { ConfigurationService, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import {
  ActivatedTableService,
  AdvancedRowActionComponent,
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent
} from '@universis/ngx-tables';
import { CalendarEvent, CalendarView } from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';
import { combineLatest, Observable, Subject, Subscription } from 'rxjs';
import { DateFormatter } from '../../advanced-date.formatter';

import { TranslateService } from '@ngx-translate/core';
import { template } from 'lodash';
import { EventsService } from '../../events.service';
import { Store, select } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { SetCalendar, SetTable, SetView } from '../../actions'
import * as fromEvents from '../../reducers/index';
import { AppEventService } from '@universis/common';

export interface TableSrcConfiguration { 
  name: string,
  tableConfigSrc: string,
  searchConfigSrc: string,
  calendarView: boolean,
  icsExport: boolean,
  filterTemplate? : string
}

export interface CalendarConfiguration {
  viewDate: Date,
  view: CalendarView,
}

const MAX_EXPORT_ITEMS = 500;

@Component({
  selector: 'universis-events',
  templateUrl: './events.component.html',
  styles: [
    `.cal-month-view .cal-open-day-events {
      padding: 15px;
      color: #000;
      background-color: #f3f3f3;
      box-shadow: inset 0 0 10px 0 rgb(137 128 128 / 50%)
    }`
  ],
  encapsulation: ViewEncapsulation.None
})
export class EventsLibComponent implements AfterViewInit, OnChanges, OnDestroy {
  private _dataSubscription: Subscription;
  private lastQuery: ClientDataQueryable;
  public filter: any = {};

  @Input() title = 'Events.Events';
  @Input() model = '';
  @Input() newModel = '';
  @Input() courseClass: any = null;
  @Input() section: any = null;
  @Input() tableView = true;
  @Input() instructor: any = null;
  @Input() timetable: number | string = '';
  @Input() sectionsEndpoint = '';
  @Input() placesEndpoint = '';
  @Input() showActions = false;
  @Input() showAdd = false;
  @Input() isAllowed = true;
  @Input() actionModel = '';
  @Input() searchConfigSrc = '';
  @Input() tableConfigSrc = '';
  @Input() commandsToEvent = ['${id}', 'attendance'];
  @Input() formTemplate = '';
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public events: Array<CalendarEvent> = [];
  public view: CalendarView = CalendarView.Month;
  public CalendarView = CalendarView;
  public viewDate: Date = new Date();
  public activeDayIsOpen = false;
  public locale: string;
  public recordsTotal: number;
  public oldClass: boolean;
  public oldTimetable: boolean;
  public tableSources: Array<TableSrcConfiguration> = [];
  public activeTableSrc : TableSrcConfiguration | undefined;
  newEventSubject: Subject<any> = new Subject();
  private selectedItems: Array<any>;
  private fragmentSubscription: Subscription;

  private activeModel: string = '';
  private enableStore: boolean;
  eventSubscription: Subscription;

  constructor(
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _eventsService: EventsService,
    private _activatedRoute: ActivatedRoute,
    private router: Router,
    private _configService: ConfigurationService,
    private _toastService: ToastService,
    private _activeTableService: ActivatedTableService,
    private _appEvent: AppEventService,
    @Optional() protected store: Store<fromEvents.EventsState>
  ) { }

  private restoreState() {
    if (this.store) {
      this.store.pipe(select(fromEvents.EventsComponentSelectors.all), take(1)).subscribe((state: fromEvents.EventsComponentState) => {
        if (state.hasOwnProperty('view')) {
          this.tableView = state.view === 'table';
        }

        if (state.calendar.hasOwnProperty('viewDate')) {
          this.viewDate = new Date(state.calendar.viewDate);
        }

        if (state.calendar.hasOwnProperty('view')) {
          this.view = state.calendar.view;
        }

        if (state.hasOwnProperty('table')) {
          this.activeTableSrc = state.table.active;
          this.tableSources = state.table.sources;
        }
      });
    }
  }
  ngAfterViewInit() {

    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment != 'reload') {
        this.activeModel = fragment;
        this.enableStore = this.activeModel && this.activeModel === 'TeachingEvents';
        if (this.enableStore === true) {
          this.restoreState();
        }
      } 
    });

    this._loadingService.showLoading();
    this.locale = this._configService.currentLocale;

    // routeData should be loaded conditionally: a non-empty model implies that
    // the component was loaded via it's selector and will trigger ngOnChagnes
    if (!this.model) {
      this._dataSubscription = combineLatest(this._activatedRoute.params, this._activatedRoute.data, this._activatedRoute.queryParams).subscribe(([params, data, queryParams]) => {
        console.log(queryParams)
        const routeData = { ...params, ...data };
        for (const [key, val] of Object.entries(routeData)) {
          // The corresponding @inputs should have default values (be initialized) for the following condition to be true.
          if (this.hasOwnProperty(key)) {
            // apply string interpolation on string inputs
            this[key] = (typeof val === 'string') ? template(val)(routeData) :  val;
          }
        }
        if (queryParams && Object.values(queryParams).length) {
          // reload the data but set table's autoLoad property to false to avoid multiple data loads (having query params triggers table load)
          this.reload(false);
        } else {
          // reload the data ensuring that the table auto loads
          this.reload(true);
        }
      });
    }

    // reload data
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        this.reload();
      }
    });
    this.eventSubscription = this._appEvent.added.subscribe((event) => {
      if (event.model === 'TeachingEvents') {
        this.reload();
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const [key, val] of Object.entries(changes)) {
      // tslint:disable-next-line: triple-equals
      if (val && val.currentValue && (val.currentValue != val.previousValue)) {
        this[key] = val.currentValue;
      }
    }
    this.reload();
  }

  reload(autoLoad = true) {
    // ensure that onSearchLoading and onTableLoading are triggered so that the table's query and search's attributes are updated
    if (this.table) this.table.reset(autoLoad);
    if (this.search) this.search.ngAfterViewInit();

    if (this.courseClass && this.courseClass.year && this.courseClass.department) {
      this.oldClass = this.courseClass && !(this.courseClass.year.id === this.courseClass.department.currentYear)
    }
    // set view date as the date of the first event of the timetable, if the timetable is from a previous year
    this._context.model('TimetableEvents')
      .where('id').equal(this.timetable)
      .select('academicYear, organizer/currentYear as currentYear')
      .getItem()
      .then(timetable => {
        if (timetable.academicYear !== timetable.currentYear) {
          this.oldTimetable = true;
          this._context.model(this.model)
            .where('superEvent').equal(this.timetable)
            .orderBy('startDate')
            .getItem()
            .then(({ startDate }) => this.viewDate = startDate)
        } else if (this.enableStore === true) {
          this.oldTimetable = false;
        } else {
          this.oldTimetable = true;
        }
      })
  }

  changeView(tableView) {
    this.tableView = tableView;

    // update the state store
    if (this.store && this.enableStore === true) {
      this.store.dispatch(new SetView(tableView ? 'table': 'calendar'));
    }

  }

  onSearchLoading({ target = null } = {}) {
    if (target && target.form) {
      target.form.timetable = this.timetable;
      target.form.model = this.model;
      target.form.sectionsEndpoint = this.sectionsEndpoint;

      // TODO: This should be handled in ngx-tables. Advanced search form component should have local translations
      // overwrite Forms translations for event status with local translations
      const currentLang = this._translateService.currentLang;
      const translation = this._translateService.instant('Events.Statuses');
      if (target.formComponent && target.formComponent.formio) Object.assign(target.formComponent.formio.i18next.options.resources[currentLang].translation, translation);
    }
  }

  onTableLoading({ target = null } = {}) {
    this._activeTableService.activeTable = this.table;

    if (target && target.config) {
      if (!this.isAllowed) {
        // tslint:disable-next-line: max-line-length
        target.config.columns = target.config.columns.filter(col => !(col.name === 'id' && (col.formatter === 'ButtonFormatter' || Array.isArray(col.formatters) && col.formatters.find(f => f.formatter === 'ButtonFormatter'))));
      }

      // make table selectable if actions are be visible
      if (this.showActions) {
        this.table.config.selectable = true;
      }
      
      if (!this.activeTableSrc) {
        // get available table sources, if any, and find and set current active table source
        if (target.config.sources && target.config.sources.length) {
          this.tableSources = target.config.sources;
          this.activeTableSrc = this.tableSources.find(table => table.tableConfigSrc === target.configSrc)
        } else {
          // else set current active table
          this.activeTableSrc = {
            name: target.config.title,
            tableConfigSrc: this.tableConfigSrc,
            searchConfigSrc: this.searchConfigSrc,
            calendarView: target.config.calendarView || true,
            icsExport: target.config.icsExport || true
          }
        }
      }

      // set filter template once for every table source
      if (!this.activeTableSrc.filterTemplate && target.config.defaults && target.config.defaults.filter) {
        this.activeTableSrc.filterTemplate = target.config.defaults.filter;
      }

      // get table filter template, process it and assign it to config
      if (this.activeTableSrc.filterTemplate) {
        target.config.defaults.filter = template(this.activeTableSrc.filterTemplate)(this);
      }

      // assign model to config
      target.config.model = this.model;

      // update the state store
      if (this.store && this.enableStore === true ) {
        this.store.dispatch(new SetTable({ active: this.activeTableSrc, sources: this.tableSources }));
      }
    }
  }

  onTableDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.lastQuery = this.table.lastQuery;

    // Data is loaded every time the search-filter changes
    // If in calendar view, reload the calendar's events
    if (!this.tableView) {
      this.createCalendarView();
    } else {
      // if in table view, hide loading, else it will be hidden after 
      // calendar view is ready 
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy() {
    if (this.fragmentSubscription && !this.fragmentSubscription.closed) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this._dataSubscription && !this._dataSubscription.closed) {
      this._dataSubscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription.unsubscribe();
    }
  }

  async newEvent(recursive = false) {
    // TODO: Filter sections that the instructor has access to and pass them
    // as an array in formProperties, to avoid access denied errors
    this._eventsService.newEvent({
      formProperties: {
        sectionsEndpoint: this.sectionsEndpoint,
        mustRegisterSection: this.courseClass && this.courseClass.mustRegisterSection,
        placesEndpoint: this.placesEndpoint
      },
      formData: { title: this.courseClass && this.courseClass.title || '' },
      formSrc: recursive ? `${this.activeModel}/newRecursive` : `${this.activeModel}/newSimple`,
      afterExecute: () => {
        try {
          this._toastService.show(
            this._translateService.instant('Events.CreatedNew'),
            this._translateService.instant('Events.CreatedSuccess'),
            true,
            3000);
          this.reload();
        } catch (error) {
          this._errorService.showError(error, {
            continueLink: '.'
          });
        }
      },
      execute: (submissionData) => {
        const parsedData =  this._eventsService.parseFormData(submissionData, {
          courseClass: this.courseClass,
          instructor: this.instructor,
          recursive
        });
        return this._context.model(this.newModel || this.model).save(parsedData);
      }
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'eventStatus/alternateName as eventStatus'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              name: item.name,
              eventStatus: item.eventStatus
            };
          });
        }
      }
    }
    return items;
  }

  async editEventStatusAction() {
    try {
      this._loadingService.showLoading();
      this.selectedItems = await this.getSelectedItems();
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.formTemplate,
          modalTitle: 'Events.EditEventStatusAction.Title',
          description: 'Events.EditEventStatusAction.Description',
          errorMessage: 'Events.EditEventStatusAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction()
        }
      });
    } catch (err) {
      console.log(err);
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  private executeChangeStatusAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const { eventStatus } = component.formComponent.form.formio.data;

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              eventStatus
            };
            await this._context.model(this.actionModel || this.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });  }

  copyAction() {

  }

  async deleteEventAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter((item) => {
        return item.eventStatus !== 'EventCompleted';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Events.DeleteAction.Title',
          description: 'Events.DeleteAction.Description',
          refresh: this.refreshAction,
          execute: this.executeDeleteEventAction()
        }
      });
    } catch (err) {
      console.log(err);
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  private executeDeleteEventAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });

            await this._context.model(this.actionModel || this.model).remove(item);
            result.success += 1;

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
        this.table.fetch();
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async createCalendarView() {
    if (!this.lastQuery) {
      return;
    }
    try {
      this._loadingService.showLoading();
      const events = await this.lastQuery.take(-1).getItems();
      if (this.isAllowed) {
        this.events = events.map(x => {
          if(x.startDate!=null && x.endDate!=null){
          return {
            id: x.id,
            title: template(this.table.config["calendarEventTitleTemplate"] || '${name}')(x),
            start: x.startDate,
            end: x.endDate,
            allDay: false,
            color: {
              primary: DateFormatter.stringToColor(x.name),
              secondary: DateFormatter.LightenDarkenColor(DateFormatter.stringToColor(x.name), 140)
            },
            actions: [
              {
                label: '<i class="fas fa-eye"></i>',
                a11yLabel: 'Preview',
                onClick: ({ event }: { event: CalendarEvent }): void => {
                  this.navigateToEvent(event);
                },
              },
            ]
          };}
        });
      } else {
        this.events = events.map(x => {
          if(x.startDate!=null && x.endDate!=null){
          return {
            title: template(this.table.config["calendarEventTitleTemplate"] || '${name}')(x),
            start: x.startDate,
            end: x.endDate,
            allDay: false,
            color: {
              primary: DateFormatter.stringToColor(x.name),
              secondary: DateFormatter.LightenDarkenColor(DateFormatter.stringToColor(x.name), 140)
            }
          };}
        });
      }
      this._loadingService.hideLoading();
      this.newEventSubject.next();
    } catch (error) {
      this.events = [];
      console.log(error);
      this._loadingService.hideLoading();
    }
  }

  public navigateToEvent(event: CalendarEvent<any>) {
    this.router.navigate(this.commandsToEvent.map(command => template(command)(event)), {
      relativeTo: this._activatedRoute,
      replaceUrl: true
    });
  }

  setView(view: CalendarView) {

    this.view = view;
    // update the state store
    if (this.store && this.enableStore === true) {
      this.store.dispatch(new SetCalendar({ view }));
    }
  }

  handleViewDateChange(viewDate) {
    this.viewDate = viewDate;

    // update the state store
    if (this.store && !this.oldTimetable && this.enableStore === true ) {
      this.store.dispatch(new SetCalendar({ viewDate }));
    }
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.activeDayIsOpen = !((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0);
      this.viewDate = date;
    }
  }

  exportCalendar() {
    const lastQuery = this.table.lastQuery;
    const tableConfig = this.table.config;
    const serviceHeaders = this._context.getService().getHeaders();
    let query = this._context.model(lastQuery.getModel()).asQueryable();
    query = this.constructQuery(query, lastQuery.getParams(), tableConfig, serviceHeaders);
    const maxExportItems = (this._configService.settings.app as any).maxExportItems;
    if (maxExportItems && typeof maxExportItems === 'number' && maxExportItems > 0) {
      query.skip(0).take(maxExportItems);
    } else {
      query.skip(0).take(MAX_EXPORT_ITEMS);
    }
    // This constructs the URI based on the way @themost does
    const url = this.constructURL(query);
    this.downloadFile(url, query.getService().getHeaders(), query.getModel()).then(r => {
      this.unsetCalendarHeader(query);
    }).catch(err => {
      console.log(err);
      this.unsetCalendarHeader(query);
    });
  }

  /**
   * Sets the parameters to the query based on
   * the active filters of the activated table
   * @param {ClientDataQueryable} query
   * @param params: The filters of the last query of the activated table
   * @return ClientDataQueryable
   */
  setParams(query: ClientDataQueryable, params): ClientDataQueryable {
    if (params && typeof params === 'object') {
      Object.keys(params).forEach((key) => {
        query.setParam(key, params[key]);
      });
    }
    return query;
  }

  /**
   * Sets the headers to the query based on
   * the default headers of the DataContext
   * and appending the accept header to get
   * an ics file from the API
   * @param {ClientDataQueryable} query
   * @param {Headers} headers: The headers to append to the query
   * @return ClientDataQueryable
   */
  setHeaders(query: ClientDataQueryable, headers: Headers): ClientDataQueryable {
    Object.keys(headers).forEach((key) => {
      if (headers.hasOwnProperty(key)) {
        query.getService().setHeader(key, headers[key]);
      }
    });
    // sets a header to accept ics files
    query.getService().setHeader('Accept', 'text/calendar');
    return query;
  }

  /**
   * Constructs the fetch URL based the query parameters
   * @param {ClientDataQueryable} query
   * @return String
   */
  constructURL(query): String {
    const baseURL = query.getService().getBase();
    let url = baseURL.endsWith('/') ? (baseURL + query.getModel() + '?') : (baseURL + '/' + query.getModel() + '?');
    Object.keys(query.getParams()).forEach((key, index) => {
      if (index === 0) {
        url += key + '=' + query.getParams()[key];
      } else {
        url += '&' + key + '=' + query.getParams()[key];
      }
    });
    return url;
  }

  /**
   * Constructs the query by setting the parameters of the
   * advanced table's active filters, sets the necessary headers
   * and build the select parameter of the query
   * @param {ClientDataQueryable} query
   * @param params: The filters of the last query of the activated table
   * @param tableConfig: The configuration of the activated table
   * @param {Headers} headers: The headers to append to the query
   * @return ClientDataQueryable
   */
  constructQuery(query, params, tableConfig, headers: Headers) {
    let _query = this.setParams(query, params);
    _query = this.setHeaders(_query, headers);
    return _query;
  }

  /**
   * Fetches and saves the xlsx file
   * @param {String} url
   * @param {Headers} serviceHeaders: The headers to append to the query
   * @param modelName: The model name of the advanced table
   */
  downloadFile(url, serviceHeaders: Headers, modelName) {
    this._loadingService.showLoading();
    return fetch(encodeURI(url), {
      headers: serviceHeaders,
      credentials: 'include'
    }).then(response => {
      if (response && !response.ok) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    }).then(blob => {
      const objectUrl = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = objectUrl;
      const downloadName = `${this.table && this.table.config ? this._translateService.instant(this.table.config.title) : modelName}.ics`;
      a.download = downloadName;
      // this adds support for IE and MS Edge (up to version 44.19041)
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, downloadName);
      } else {
        // for all other browsers
        a.click();
      }
      window.URL.revokeObjectURL(objectUrl);
      a.remove();
      this._loadingService.hideLoading();
    }).catch(err => {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  unsetCalendarHeader(query: ClientDataQueryable): void {
    if (query.getService().getHeaders()['Accept'] === 'text/calendar') {
      query.getService().setHeader('Accept', 'application/json');
    }
  }


}
