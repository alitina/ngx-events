import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import {
  ActivatedTableService,
  AdvancedFilterValueProvider,
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableDataResult, AdvancedTableSearchComponent
} from '@universis/ngx-tables';
import { ErrorService, LoadingService } from '@universis/common';
import { ClientDataQueryable, ResponseError } from '@themost/client';
import { TranslateService } from '@ngx-translate/core';

interface LocationEvent {
  id: number;
  location: number;
  name: string;
  eventStatus: string;
  startDate: Date;
  endDate: Date;
  additionalType: string;
  organizerName: string;
  performerName: string;
  eventHoursSpecification: number;
  performer: {
    alternateName: string;
  };
}

@Component({
  selector: 'universis-location-events-table',
  templateUrl: './location-events-table.component.html'
})
export class LocationEventsTableComponent implements OnInit, OnDestroy {
  private _paramSubscription: Subscription;
  private _dataSubscription: Subscription;
  public recordsTotal: number;
  public tableConfigSrc: any;
  public searchConfigSrc: any;
  public events: Array<LocationEvent>;
  public viewMode = 'table';
  public listTimeType: string;
  public listRepeatFilter: string;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _loadingService: LoadingService,
    private _advancedFilterValueProvider: AdvancedFilterValueProvider,
    private _activatedTable: ActivatedTableService,
    private _translateService: TranslateService,
    private _errorService: ErrorService
  ) {}

  ngOnInit(): void {
    this._paramSubscription = this._activatedRoute.params.subscribe(async params => {
      if (!this._advancedFilterValueProvider.values.hasOwnProperty('locationId') ||
        this._advancedFilterValueProvider.values['locationId'] !== params.id) {
        this._advancedFilterValueProvider.values['locationId'] = params.id;
      }

      this.listTimeType = params.list;
      this.listRepeatFilter = params.type;
    });
    this._dataSubscription = this._activatedRoute.data.subscribe(async data => {
      try {
        if (data.searchConfiguration) {
          this.searchConfigSrc = data.searchConfiguration;
        }
        if (data.tableConfiguration) {
          this.tableConfigSrc = data.tableConfiguration;
        }
        this.viewMode = 'table';
      } catch (error) {
        console.error(error);
      }
    });
  }

  /**
   * On click function that ensures that the activeTable is set, since it is required by the universis-btn-export directive
   */
  activateTable(): void {
    this._activatedTable.activeTable = this.table;
  }

  onDataLoad(data: AdvancedTableDataResult): void {
    this.transformEventData(data.data as Array<LocationEvent>);
    this.recordsTotal = data.recordsTotal;
    this._loadingService.hideLoading();
  }

  /**
   * Function that transforms the given events so that they are able displayed properly in EventsCalendarComponent
   * @param events The events that will get passed to the EventsCalendarComponent
   */
  transformEventData(events: Array<LocationEvent>): void {
    this.events = events.filter(event => event.eventHoursSpecification === null);
    this.events.forEach((event) => {
      event.performer = {
        alternateName: event.performerName
      };
    });
  }

  exportCalendar(): void {
    const query: ClientDataQueryable = this.table.lastQuery;
    const queryURL: string = this.constructURL(query);
    const headers: Headers = {
      ...query.getService().getHeaders(),
      'Accept': 'text/calendar',
      'Content-Type': 'text/calendar'
    };

    fetch(encodeURI(queryURL), {
      headers: headers,
      credentials: 'include'
    }).then(response => {
      if (response && !response.ok) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    }).then(blob => {
      const objectUrl = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = objectUrl;
      // tslint:disable-next-line:max-line-length
      const downloadName = `${this.table && this.table.config ? this._translateService.instant(this.table.config.title) : query.getModel()}.ics`;
      a.download = downloadName;
      // this adds support for IE and MS Edge (up to version 44.19041)
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, downloadName);
      } else {
        // for all other browsers
        a.click();
      }
      window.URL.revokeObjectURL(objectUrl);
      a.remove();
      this._loadingService.hideLoading();
    }).catch(err => {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });

  }

  /**
   * Constructs the fetch URL based the query parameters
   * @param {ClientDataQueryable} query
   * @return String
   */
  constructURL(query: ClientDataQueryable): string {
    const baseURL = query.getService().getBase();
    let url = baseURL.endsWith('/') ? (baseURL + query.getModel() + '?') : (baseURL + '/' + query.getModel() + '?');
    Object.keys(query.getParams()).forEach((key, index) => {
      if (index === 0) {
        url += key + '=' + query.getParams()[key];
      } else {
        url += '&' + key + '=' + query.getParams()[key];
      }
    });
    return url;
  }

  ngOnDestroy(): void {
    if (this._paramSubscription) {
      this._paramSubscription.unsubscribe();
    }
    if (this._dataSubscription) {
      this._dataSubscription.unsubscribe();
    }
  }
}
