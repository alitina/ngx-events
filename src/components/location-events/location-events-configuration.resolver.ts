import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocationEventsTableConfigurationResolver implements Resolve<String> {
  constructor(private httpClient: HttpClient) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {
    const configSrc = `assets/lists/LocationEvents/${route.params.list}.${route.params.type}.config.json`;
    const defaultConfigSrc = 'assets/lists/LocationEvents/upcoming.all.config.json';
    return this.httpClient.get(configSrc).pipe(map(response => configSrc), catchError(error => of(defaultConfigSrc)));
  }
}

@Injectable({
  providedIn: 'root'
})
export class LocationEventsSearchConfigurationResolver implements Resolve<String> {
  constructor(private httpClient: HttpClient) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {
    const configSrc = `assets/lists/LocationEvents/${route.params.list}.search.json`;
    const defaultConfigSrc = 'assets/lists/LocationEvents/upcoming.search.json';
    return this.httpClient.get(configSrc).pipe(map(response => configSrc), catchError(error => of(defaultConfigSrc)));
  }
}
