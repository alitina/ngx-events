import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-timetable-home',
  templateUrl: './timetable-home.component.html'
})
export class TimetableHomeComponent implements OnInit, OnDestroy {


  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  public tabs: Array<any> = [];
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _httpClient: HttpClient) { }

  ngOnInit() {
    this._httpClient.get('assets/lists/TimetableEvents/all.json').subscribe((config: any) => {
      this.paths = config.paths;
      this.activePaths = this.paths.filter(x => {
        return x.show === true;
      }).slice(0);
      this.paramSubscription = this._activatedRoute.firstChild.params.subscribe(params => {
        const matchPath = new RegExp('^list/' + params.list + '$', 'ig');
        const findItem = this.paths.find(x => {
          return matchPath.test(x.alternateName);
        });
        if (findItem) {
          this.showTab(findItem);
        }
      });
    });
  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex(x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

}
