import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalService, LoadingService } from '@universis/common';
import { ButtonTypes } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-timetable-print',
  templateUrl: './timetable-print.component.html'
})
export class TimetablePrintComponent implements OnInit, OnDestroy {
  private formLoadSubscription: Subscription;
  private formChangeSubscription: Subscription;

  public lastError;
  public okButtonDisabled = true;

  @Input() okButtonText = ButtonTypes.ok.buttonText;
  @Input() okButtonClass = ButtonTypes.ok.buttonClass;
  @Input() cancelButtonText = ButtonTypes.cancel.buttonText;
  @Input() cancelButtonClass = ButtonTypes.cancel.buttonClass;
  @Input() showButtons = true;
  @Input() execute: Observable<any>;
  @Input() formSrc = 'TimetableEvents/print';
  @Input() formData = {};
  @Input() formProperties = {};
  @Input() navigate: () => any;

  @ViewChild('formComponent') formComponent: AdvancedFormComponent;

  constructor(private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _translateService: TranslateService) {
  }

  ngOnInit() {
    this.formLoadSubscription = this.formComponent.form.formLoad.subscribe((formConfig) => {
      if (formConfig) {
        // find submit button
        const findButton = this.formComponent.form.form.components.find(component => {
          return component.type === 'button' && component.key === 'submit';
        });
        // hide button
        if (findButton) {
          (<any>findButton).hidden = true;
        }
        //  add formProperties on first load (delete formProperties to avoid infinite event loop)
        if (this.formProperties) {
          // apply changes to form config (reassigning the formConfig will force the form to rerender with the updated config)
          this.formComponent.formConfig = { ...formConfig, ...this.formProperties };
          delete this.formProperties;
        }
      }
    });
    this.formChangeSubscription = this.formComponent.form.change.subscribe((event) => {
      if (this.formComponent.form.formio) {
        // enable or disable button based on form status
        // this.okButtonDisabled = !event.isValid;

        // When the form is first loaded, the event returns "isValid : true" even though the studyProgram, reportTemplate required fields are empty.
        // Use formio.checkvalidity() instead (https://help.form.io/developers/form-renderer#form.checkvalidity-data-dirty-row-silent)
        this.okButtonDisabled = !this.formComponent.form.formio.checkValidity(null, false, null, true);
        this.lastError = null;
      }
    });
  }

  close() {
    // close
    if (this._modalService.modalRef) {
      this._modalService.modalRef.hide();
    }

    // continue navigation
    if (typeof this.navigate === 'function') {
      return this.navigate();
    }
  }

  ok() {
    this.lastError = null;
    this._loadingService.showLoading();
    this.execute.subscribe((result) => {
      this._loadingService.hideLoading();
      this.close()
    }, (err) => {
      this._loadingService.hideLoading();
      this.lastError = this.translateError(err.error);
    });
  }

  translateError(error) {
    const title = this._translateService.instant('Events.SubmitError');
    if (!error || !error.message) { return { title: title }; }

    let messageToTranslate = 'Events.Errors.' + error.message;
    let message = this._translateService.instant(messageToTranslate);

    // if translation failed show a generic message
    if (message === messageToTranslate) {
      messageToTranslate = 'E' + (error.statusCode || 500) + '.message';
      message = this._translateService.instant(messageToTranslate);
    }

    return {
      title: title,
      message: message
    };
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }
}

