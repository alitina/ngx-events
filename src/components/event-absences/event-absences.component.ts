import {Component, OnInit, ViewChild, Injector, Input, OnDestroy} from '@angular/core';
import {EVENT_ABSENCES_CONFIGURATION} from './event-absences.configuration';
import {EVENT_ABSENCES_SEARCH_CONFIGURATION} from './event-absences.search';
import { ErrorService, LoadingService, ModalService, ToastService, ConfigurationService, AppEventService } from '@universis/common';
import { AngularDataContext } from '@themost/angular';
import {
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent
} from '@universis/ngx-tables';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { DateFormatter } from '../../advanced-date.formatter';
import { template } from 'lodash';

declare var $: any;
@Component({
  selector: 'universis-event-absences',
  templateUrl: './event-absences.component.html',
  styleUrls: ['./event-absences.component.scss']
})
export class EventAbsencesComponent implements OnInit, OnDestroy {
  private _router: Router;
  private _activatedRoute: ActivatedRoute;
  private _paramSubscription: Subscription;
  private _toastService: ToastService;

  // Each set holds the ids of the students that were initially absent/present and were changed to present/absent
  private studentsLeft: Set<string> = new Set();
  private studentsRight: Set<string> = new Set();
  private _eventService: AppEventService;

  // Getter that represents the number of students whose present/absent status was modified
  public get studentsModified() {
    return this.studentsLeft.size + this.studentsRight.size;
  }

  public event;
  public tableConfiguration: any;
  public searchConfiguration: any = EVENT_ABSENCES_SEARCH_CONFIGURATION;
  public recordsTotal: number;
  public formattedDate: any = {};
  public showButtons = false;
  public isLoading = true;
  public back = true;

  @Input() _model = '';
  @Input() eventModel = '';
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor(
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _injector: Injector,
    private _translateService: TranslateService
  ) { }

  setDate() {
    const currLocale = this._injector.get(ConfigurationService).currentLocale;
    const datePipe: DatePipe = new DatePipe(currLocale);
    this.formattedDate.dot = DateFormatter.stringToColor(this.event.name);
  }

  loadEvent(eventID) {
    this._loadingService.showLoading();
    this.isLoading = true;
    return this._context.model(this.eventModel)
      .where('id')
      .equal(eventID)
      // tslint:disable-next-line: max-line-length
      .expand('courseClass($expand=department;$select=id,year as year,department/currentYear as currentYear, absenceLimit)')
      .getItem()
      .then((event) => {
        this.event = event;
        this.showButtons =
          event.courseClass &&
          event.courseClass.year === event.courseClass.currentYear &&
          event.eventStatus.name === 'EventOpened';
        // Hide the buttons column in view mode
        if (!this.showButtons) {
          this.tableConfiguration.columns = EVENT_ABSENCES_CONFIGURATION.columns.filter(col => col.name !== 'attendanceButton');
        }
        this.setDate();
        this.isLoading = false;
        this._loadingService.hideLoading();
      }).catch(err => {
        this.isLoading = false;
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }

  ngOnInit() {
    this._router = this._injector.get(Router);
    this._activatedRoute = this._injector.get(ActivatedRoute);
    this._toastService = this._injector.get(ToastService);
    this._eventService = this._injector.get(AppEventService);
    this._paramSubscription = combineLatest(this._activatedRoute.params, this._activatedRoute.data).subscribe(([params, data]) => {
      this.showButtons = false;
      const eventID = params.id;

      if (data && data._model) {
        this._model = template(data._model)({ ...params, ...data });
        this.eventModel = data.eventModel;
        // deep copy the table configuration so that the original isn't affected
        this.tableConfiguration = JSON.parse(JSON.stringify(EVENT_ABSENCES_CONFIGURATION));
        this.tableConfiguration.model = this._model;
        this.searchConfiguration.model = this._model;

        // TODO: Temp fix: this.back is used to show/hide backToEvents button. Address this properly.
        if (data.hasOwnProperty('back')) { this.back = data.back; }

        // event target: timetable-dashboard component;
        this.loadEvent(eventID).then(() => this._eventService.change.next({
          model: this.event.additionalType,
          target: this.event,
          action: 'preview'
        }));
      }
    });
  }

  ngOnDestroy() {
    if (this._paramSubscription && !this._paramSubscription.closed) {
      this._paramSubscription.unsubscribe();
    }
  }

  onTableInit() {
    this._loadingService.showLoading();
  }

  onTableDataLoad(data: AdvancedTableDataResult) {
    // hide loading fired on table initialization
    this._loadingService.hideLoading();
    // update recordsTotal
    this.recordsTotal = data.recordsTotal;
    // this additional data is used to determine the visibility and style of the attendanceButton in advanced-absence.formatter.ts
    data.data.forEach(row => {
      const hasRecord = row.attendances.filter(record => record.event === this.event.id).length > 0;
      row.presenceType = this.event.presenceType;
      row.absent = this.event.presenceType ? !hasRecord : hasRecord;
    });
  }

  onTablePageChange() {
    function toggleButtonClasses(students, side) {
      const thisButtonId = (side === 'left') ? '#leftButton' : '#rightButton';
      const oppositeButtonId = (side === 'left') ? '#rightButton' : '#leftButton';

      students.forEach(studentId => {
        const buttonGroup = $(`#${studentId}`);
        if (buttonGroup) {
          const button = buttonGroup.find(thisButtonId);
          const oppositeButton = buttonGroup.find(oppositeButtonId);
          button.addClass('active');
          oppositeButton.removeClass('active');

          // mark modified rows
          const row = button.closest('tr');
          row.addClass('text-info font-weight-bold');
        }
      });
    }

    function setClickEvent(button, buttonOppositeSide, studentsSide, studentsOppositeSide, studentId) {
      const buttonEvents = $._data(button.get(0), 'events');
      const hasClickEvents = buttonEvents && buttonEvents.hasOwnProperty('click');

      if (!hasClickEvents) {
        button.on('click', (event: Event) => {
          event.preventDefault();
          event.stopPropagation();

          const switchedBackOrPressedAgain = button.hasClass('active') || studentsOppositeSide.has(studentId);
          studentsOppositeSide.delete(studentId);

          // The present/absent status of a student that belongs to the opposite set is not their initial status.
          // Since this button is pressed, such a student's status is reset to its original state.
          // So if the button was already active or the student's status is reset, don't add the student to the set
          const row = button.closest('tr');          
          if (!switchedBackOrPressedAgain) {
            studentsSide.add(studentId);
            row.addClass('text-info font-weight-bold'); // mark modified rows
          } else {
            row.removeClass('text-info font-weight-bold'); // un-mark unmodified rows
          }

          // make clicked button active and opposite button inactive
          button.addClass('active')
          buttonOppositeSide.removeClass('active')
        });
      }
    }

    if (this.showButtons) {
      // set click events for all buttons that are currently present in the pages view
      $('.btn-group.btn-group-toggle').each((index, element) => {
        const studentId = element.id;
        const leftBtn = $(element).find('#leftButton');
        const rightBtn = $(element).find('#rightButton');

        setClickEvent(leftBtn, rightBtn, this.studentsLeft, this.studentsRight, studentId);
        setClickEvent(rightBtn, leftBtn, this.studentsRight, this.studentsLeft, studentId);
      });

      // When a new page is loaded the html is rerendered and the 'active' class is removed from the pressed buttons.
      // Reassign the 'active' class to the pressed buttons.
      if (this.studentsModified) {
        toggleButtonClasses(this.studentsLeft, 'left');
        toggleButtonClasses(this.studentsRight, 'right');
      }
    }

  }

  clearStudentSets() {
    this.studentsLeft.clear();
    this.studentsRight.clear();
  }

  getAllStudents() {
    return this._context.model(this._model)
      .where('student/studentStatus/alternateName').equal('active')
      .take(-1)
      .getItems();
  }

  getAttendanceToPost(student, state = 'add') {
    const attendance: any = {
      event: this.event.id,
      student: student.student,
      studentCourseClass: student.id
    };

    if (state === 'delete') { attendance.$state = 4; }
    return attendance;
  }

  async allPresent() {
    this._modalService.showWarningDialog(
      this._translateService.instant('Events.Warning'),
      this._translateService.instant('Events.DeleteAbsencesCheck')).then( async result => {
        if (result === 'ok') {
          try {
            this._loadingService.showLoading();
            const attendanceList = [];
            const students = await this.getAllStudents();

            // in case of an presenceEvent all students must have a record
            // in case of a absenceEvent all students must NOT have a record
            students.forEach(student => {
              const hasRecord = student.attendances.filter(record => record.event === this.event.id).length > 0;
              const wasAbsent = this.event.presenceType ? !hasRecord : hasRecord;

              if (wasAbsent && hasRecord) { attendanceList.push(this.getAttendanceToPost(student, 'delete')); }
              if (wasAbsent && !hasRecord) { attendanceList.push(this.getAttendanceToPost(student)); }
            });

            // save the attendance list and clear the student sets
            await this._context.model(this._model).save(attendanceList);
            this.clearStudentSets();

            this._loadingService.hideLoading();
            this._toastService.show(
              this._translateService.instant('Events.ActionCompleted'),
              this._translateService.instant('Events.AllSavedPresent'),
              true,
              2000);

            // reload the table
            this.table.fetch();
          } catch (err) {
            console.log(err);
            this._loadingService.hideLoading();
            this._errorService.showError(err, {
              continueLink: '.'
            });
          }
        }
    });
  }

  async save() {
    if (!this.studentsModified) { return; }
    try {
      this._loadingService.showLoading();
      const translation = this.event.presenceType ? 'Events.SavedPresent' : 'Events.SavedAbsent';
      const students = await this.getAllStudents();
      const attendanceList = [];

      // Students of the left set are now present in case of a presenceEvent and absent in case of presenceEvent.
      // In both cases, an attendance record should be added.
      this.studentsLeft.forEach(studentId => {
        const student = students.find(stud => String(stud.student) === String(studentId));
        attendanceList.push(this.getAttendanceToPost(student));
      });

      // Students of the right set are now absent in case of a presenceEvent and present in case of presenceEvent.
      // In both cases, their attendance record should be deleted.
      this.studentsRight.forEach(studentId => {
        const student = students.find(stud => String(stud.student) === String(studentId));
        attendanceList.push(this.getAttendanceToPost(student, 'delete'));
      });

      // save the attendance list and clear the student sets
      await this._context.model(this._model).save(attendanceList);
      this.clearStudentSets();


      this._loadingService.hideLoading();
      this._toastService.show(
        this._translateService.instant('Events.ActionCompleted'),
        this._translateService.instant(translation),
        true,
        2000);

      // reload the table
      this.table.fetch();
    } catch (err) {
      console.log(err);
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  async commit() {
    // TODO: Add second dialog when student statuses have change in order to confirm if they should be saved or not
    this._modalService.showWarningDialog(
      this._translateService.instant('Events.Warning'),
      this._translateService.instant('Events.CommitCheck'))
      .then( async result => {
        if (result === 'ok') {
          try {
            this._loadingService.showLoading();
            // close the event and clear the student sets
            await this._context.model(this._model.replace('attendance', 'close')).save([]);
            this.clearStudentSets();

            // reload the event (the table is also reloaded) to hide the buttons
            await this.loadEvent(this.event.id);

            this._loadingService.hideLoading();
            this._toastService.show(
              this._translateService.instant('Events.ActionCompleted'),
              this._translateService.instant('Events.MarkedCompleted'),
              true,
              2000);
          } catch (err) {
            console.log(err);
            this._loadingService.hideLoading();
            this._errorService.showError(err, {
              continueLink: '.'
            });
          }
        }
      });
  }
}
