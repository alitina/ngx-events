## @universis/ngx-events

`@universis/ngx-events` is an implementation of an event scheduling system
used by universis client applications.

### Installation

    npm i @universis/ngx-events

### Usage 

Import `EventsModule` in app modules

    ...
    import { EventsModule } from '@universis/ngx-events';

    @NgModule({
    declarations: [
        ...
    ],
    imports: [
        ...
        EventsModule
    ],
        providers: [
            ...
        ],
        bootstrap: [ AppComponent ]
    })
    export class AppModule {

    }

### Development

This project is intended to be used as a part of an angular cli project. 

- Add this project as git submodule

        git submodule add https://gitlab.com/universis/ngx-events.git packages/ngx-events

- Edit angular.json and add `ngx-events` configuration under `packages` node:

        "ngx-events": {
            "root": "packages/ngx-events",
            "sourceRoot": "packages/ngx-events/src",
            "projectType": "library",
            "prefix": "universis",
            "architect": {
                "build": {
                    "builder": "@angular-devkit/build-ng-packagr:build",
                    "options": {
                        "tsConfig": "packages/ngx-events/tsconfig.lib.json",
                        "project": "packages/ngx-events/ng-package.json"
                    }
                },
                "test": {
                "builder": "@angular-devkit/build-angular:karma",
                "options": {
                    "main": "packages/ngx-events/src/test.ts",
                    "tsConfig": "packages/ngx-events/tsconfig.spec.json",
                    "karmaConfig": "packages/ngx-events/karma.conf.js"
                }
                },
                "lint": {
                "builder": "@angular-devkit/build-angular:tslint",
                "options": {
                    "tsConfig": [
                        "packages/ngx-events/tsconfig.lib.json",
                        "packages/ngx-events/tsconfig.spec.json"
                    ],
                    "exclude": [
                    "**/node_modules/**"
                    ]
                }
            }
        }
        }

- Import `@universis/ngx-events` in tsconfig.json#paths

        {
            ...
            "compilerOptions": {
                ...
                "paths": {
                    ...
                    "@universis/ngx-events": [
                        "packages/ngx-events/src/public_api"
                    ]
                }
            }
        }


